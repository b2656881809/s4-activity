package com.zuitt.wdc044.services;


import com.zuitt.wdc044.models.Post;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;

public interface PostService {

    //create a post
    void createPost(String stringToken, Post post);

    //getting all post
    Iterable<Post> getPosts();

    ResponseEntity updatePost(Long id, String stringToken, Post post);

    //delete a post
    ResponseEntity deletePost(Long id, String stringToken);

    ResponseEntity getMyPosts(String stringToken);


}
